-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.39 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных employee
DROP DATABASE IF EXISTS `employee`;
CREATE DATABASE IF NOT EXISTS `employee` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `employee`;


-- Дамп структуры для таблица employee.employee
DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `second_name` varchar(50) NOT NULL,
  `age` smallint(3) unsigned NOT NULL,
  `experience` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы employee.employee: ~3 rows (приблизительно)
DELETE FROM `employee`;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`employee_id`, `first_name`, `last_name`, `second_name`, `age`, `experience`, `description`) VALUES
	(2, 'ahagfdhs', 'hgjhg', 'jhgjhgjhg', 65, 'gfsxkxsk', 'vnbvmnbvmnbv'),
	(3, 'hcfhgfhg', 'fhgfhgf', 'test', 56, 'yuiyuyiuyi', '8879879'),
	(4, 'jkkk', 'kjkj', 'jhgjhgjhg', 5, 'gfsxkxsk', '8879879');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
