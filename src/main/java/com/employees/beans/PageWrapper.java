package com.employees.beans;


import java.util.ArrayList;
import java.util.List;

public class PageWrapper {
    private static final int MAX_PAGE_DISPLAY = 10;
    private static final int MAX_EMPLOYEES_PER_PAGE_DISPLAY = 50;
    private final List<Integer> pagesList;
    private final int pagesCount;

    public PageWrapper(int listSize, int page) {
        
        pagesCount = (int) Math.ceil(listSize / (double) MAX_EMPLOYEES_PER_PAGE_DISPLAY);
        pagesList = new ArrayList<>();
        int start, size;

        if (pagesCount <= MAX_PAGE_DISPLAY) {
            start = 1;
            size = pagesCount;
        } else {
            if (page <= MAX_PAGE_DISPLAY - MAX_PAGE_DISPLAY / 2) {
                start = 1;
                size = MAX_PAGE_DISPLAY;
            } else if (page > pagesCount - MAX_PAGE_DISPLAY / 2) {
                start = pagesCount - MAX_PAGE_DISPLAY + 1;
                size = MAX_PAGE_DISPLAY;
            } else {
                start = page - MAX_PAGE_DISPLAY / 2;
                size = MAX_PAGE_DISPLAY;
            }
        }
        for (int i = start; i < size + start; i++) {
            pagesList.add(i);
        }
    }

    public List<Integer> getPages() {
        return pagesList;
    }
    
    public int getPagesCount() {
        return pagesCount;
    }
}
