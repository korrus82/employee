package com.employees.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.employees.beans.Employee;
import com.employees.beans.PageWrapper;
import com.employees.services.EmployeesService;

@Controller
public class EmployeesController {

    @Autowired
    private EmployeesService employeesService;

    @RequestMapping(value = "/index.action", method = RequestMethod.GET)
    public ModelAndView getIndex(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "sFirstName", required = false) String sFirstName, @RequestParam(value = "sLastName", required = false) String sLastName,
            @RequestParam(value = "sSecondName", required = false) String sSecondName, @RequestParam(value = "sAge", required = false) Integer sAge,
            @RequestParam(value = "sExperience", required = false) String sExperience,
            @RequestParam(value = "sDescription", required = false) String sDescription,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

        ModelAndView mav = new ModelAndView("index");

        List<Employee> employeesList = employeesService.getEmployeesList(page, sFirstName, sLastName, sSecondName, sAge, sExperience, sDescription);

        PageWrapper pageWrapper = new PageWrapper(employeesList.size(), page);

        mav.addObject("employeesList", employeesList);
        mav.addObject("pageWrapper", pageWrapper);
        mav.addObject("sFirstName", sFirstName);
        mav.addObject("sLastName", sLastName);
        mav.addObject("sSecondName", sSecondName);
        mav.addObject("sAge", sAge);
        mav.addObject("sExperience", sExperience);
        mav.addObject("sDescription", sDescription);
        mav.addObject("page", page);

        return mav;
    }

    @RequestMapping(value = "/add.action", method = RequestMethod.POST)
    public ModelAndView addEmployee(HttpServletResponse response, @RequestParam(value = "firstName", required = true) String firstName,
            @RequestParam(value = "lastName", required = true) String lastName, @RequestParam(value = "secondName", required = true) String secondName,
            @RequestParam(value = "age", required = true) int age, @RequestParam(value = "experience", required = true) String experience,
            @RequestParam(value = "description", required = true) String description) {

        Employee employee = new Employee();

        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setSecondName(secondName);
        employee.setAge(age);
        employee.setExperience(experience);
        employee.setDescription(description);

        employeesService.addEmployee(employee);
        return new ModelAndView("redirect:index.action");
    }

    @RequestMapping(value = "/delete.action", method = RequestMethod.POST)
    public ModelAndView deleteEmployee(HttpServletResponse response, @RequestParam(value = "employeeId", required = true) int employeeId) {

        employeesService.deleteEmployee(employeeId);
        return new ModelAndView("redirect:index.action");
    }

    @RequestMapping(value = "/update.action", method = RequestMethod.POST)
    public void updateEmployee(HttpServletResponse response, @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "pk", required = true) int pk, @RequestParam(value = "value", required = true) String value) {

        employeesService.updateEmployee(name, pk, value);
    }

}