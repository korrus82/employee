package com.employees.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.employees.beans.Employee;
import com.employees.dao.EmployeesDao;

@Service
public class EmployeesService {

    @Autowired
    private EmployeesDao employeesDao;

    @Transactional
    public List<Employee> getEmployeesList(int page, String sFirstName, String sLastName, String sSecondName, Integer sAge, String sExperience, String sDescription) {
        return employeesDao.getEmployeesList(page, sFirstName, sLastName, sSecondName, sAge, sExperience, sDescription);
    }

    @Transactional
    public void addEmployee(Employee employee) {
        employeesDao.addEmployee(employee);
    }

    @Transactional
    public void deleteEmployee(int employeeId) {
        employeesDao.deleteEmployee(employeeId);
    }

    @Transactional
    public void updateEmployee(String name, int pk, String value) {
        employeesDao.updateEmployee(name, pk, value);
    }
}
