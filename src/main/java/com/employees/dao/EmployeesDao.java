package com.employees.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.employees.beans.Employee;

@SuppressWarnings("unchecked")
@Repository
public class EmployeesDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Employee> getEmployeesList(int page, String sFirstName, String sLastName, String sSecondName, Integer sAge, String sExperience, String sDescription) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);

        if (sFirstName != null) {
            criteria.add(Restrictions.like("firstName", sFirstName, MatchMode.ANYWHERE));
        }
        
        if (sLastName != null) {
            criteria.add(Restrictions.like("lastName", sLastName, MatchMode.ANYWHERE));
        }
        
        if (sSecondName != null) {
            criteria.add(Restrictions.like("secondName", sSecondName, MatchMode.ANYWHERE));
        }
        
        if (sAge != null) {
            criteria.add(Restrictions.eq("age", sAge));
        }

        if (sExperience != null) {
            criteria.add(Restrictions.like("experience", sExperience, MatchMode.ANYWHERE));
        }
  
        if (sDescription != null) {
            criteria.add(Restrictions.like("description", sDescription, MatchMode.ANYWHERE));
        }
        
        criteria.setFirstResult((page - 1) * 50);
        criteria.setMaxResults(50);
        // List<Item> itemsList = criteria.list();
        return criteria.list();
    }

    public void addEmployee(Employee employee) {
        sessionFactory.getCurrentSession().save(employee);
        sessionFactory.getCurrentSession().flush();
    }

    public void deleteEmployee(int employeeId) {
        sessionFactory.getCurrentSession().createQuery("DELETE Employee WHERE employeeId = :employeeId").setParameter("employeeId", employeeId).executeUpdate();
    }

    public void updateEmployee(String name, int pk, String value) {

        switch (name) {
        case "updateFirstName":
            System.out.println(name);
            System.out.println(pk);
            System.out.println(value);
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET firstName = :firstName WHERE employeeId = :employeeId")
                    .setParameter("employeeId", pk).setParameter("firstName", value).executeUpdate();
            break;
        case "updateLastName":
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET lastName = :lastName WHERE employeeId = :employeeId")
                    .setParameter("employeeId", pk).setParameter("lastName", value).executeUpdate();
            break;
        case "updateSecondName":
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET secondName = :secondName WHERE employeeId = :employeeId")
                    .setParameter("employeeId", pk).setParameter("secondName", value).executeUpdate();
            break;
        case "updateAge":
            int age = Integer.parseInt(value);
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET age = :age WHERE employeeId = :employeeId").setParameter("age", age)
                    .setParameter("employeeId", pk).executeUpdate();
            break;
        case "updateExperience":
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET experience = :experience WHERE employeeId = :employeeId")
                    .setParameter("employeeId", pk).setParameter("experience", value).executeUpdate();
            break;
        case "updateDescription":
            sessionFactory.getCurrentSession().createQuery("UPDATE Employee SET description = :description WHERE employeeId = :employeeId")
                    .setParameter("employeeId", pk).setParameter("description", value).executeUpdate();
            break;
        default:
            break;
        }

    }
}
