if (typeof jQuery === 'undefined') {
	throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function($) {
	'use strict';

	// CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
	// ============================================================
	$(document).ready(function() {
		// toggle `popup` / `inline` mode
		$.fn.editable.defaults.mode = 'popup';

		$('#employees a').editable({
			url : 'update.action',
			type : 'text',
			title : 'Enter your text'		
		});
		
//		// make username editable
//		$('#updateFirstName').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateFirstName',
//			title : 'Enter your text'
//		});
//		$('#updateLastName').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateLastName',
//			title : 'Enter your text'
//		});
//		$('#updateSecondName').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateSecondName',
//			title : 'Enter your text'
//		});
//		$('#updateAge').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateAge',
//			title : 'Enter your text'
//		});
//		$('#updateExperience').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateExperience',
//			title : 'Enter your text'
//		});
//		$('#updateDescription').editable({
//			url : 'update.action',
//			type : 'text',
//			name : 'updateDescription',
//			title : 'Enter your text'
//		});
	});

	$(document)
			.ready(
					function() {
						$('#registerform')
								.bootstrapValidator(
										{
											// live : 'enabled',
											message : 'This value is not valid',
											feedbackIcons : {
												valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											fields : {

												firstName : {
													message : 'The firstName is not valid',
													validators : {
														notEmpty : {
															message : 'The firstName is required and cannot be empty'
														},
														stringLength : {
															min : 3,
															max : 50,
															message : 'The firstName must be more than 3 and less than 50 characters long'
														},
														regexp : {
															regexp : /^[a-zA-Z]+$/,
															message : 'The firstName can only consist of alphabetical'
														}
													}
												},
												lastName : {
													message : 'The lastName is not valid',
													validators : {
														notEmpty : {
															message : 'The lastName is required and cannot be empty'
														},
														stringLength : {
															min : 3,
															max : 50,
															message : 'The lastName must be more than 3 and less than 50 characters long'
														},
														regexp : {
															regexp : /^[a-zA-Z]+$/,
															message : 'The lastName can only consist of alphabetical'
														}
													}
												},
												secondName : {
													message : 'The secondName is not valid',
													validators : {
														notEmpty : {
															message : 'The secondName is required and cannot be empty'
														},
														stringLength : {
															min : 3,
															max : 50,
															message : 'The secondName must be more than 3 and less than 50 characters long'
														},
														regexp : {
															regexp : /^[a-zA-Z]+$/,
															message : 'The secondName can only consist of alphabetical'
														}
													}
												},
												age : {
													message : 'The age is not valid',
													validators : {
														notEmpty : {
															message : 'The age is required and cannot be empty'
														},
														stringLength : {
															min : 1,
															max : 3,
															message : 'The age must be more than 1 and less than 3 characters long'
														},
														regexp : {
															regexp : /^[0-9]+$/,
															message : 'The age can only consist of  number'
														}
													}
												},
												experience : {
													message : 'The experience is not valid',
													validators : {
														notEmpty : {
															message : 'The experience is required and cannot be empty'
														},
														stringLength : {
															min : 1,
															max : 100,
															message : 'The agexperiencee must be more than 1 and less than 100 characters long'
														},
														regexp : {
															regexp : /^[a-zA-Z0-9_]+$/,
															message : 'The experience can only consist of alphabetical, number and underscore'
														}
													}
												},
												description : {
													message : 'The experience is not valid',
													validators : {
														notEmpty : {
															message : 'The experience is required and cannot be empty'
														},
														stringLength : {
															min : 1,
															max : 100,
															message : 'The agexperiencee must be more than 1 and less than 100 characters long'
														},
														regexp : {
															regexp : /^[a-zA-Z0-9_]+$/,
															message : 'The experience can only consist of alphabetical, number and underscore'
														}
													}
												}

											}
										});
					});

	$(document)
	.ready(
			function() {
				$('#searchform')
						.bootstrapValidator(
								{
									// live : 'enabled',
									message : 'This value is not valid',
									feedbackIcons : {
										valid : 'glyphicon glyphicon-ok',
										invalid : 'glyphicon glyphicon-remove',
										validating : 'glyphicon glyphicon-refresh'
									},
									fields : {

										sFirstName : {
											message : 'The firstName is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 50,
													message : 'The firstName must be more than 3 and less than 50 characters long'
												},
												regexp : {
													regexp : /^[a-zA-Z]+$/,
													message : 'The firstName can only consist of alphabetical'
												}
											}
										},
										sLastName : {
											message : 'The lastName is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 50,
													message : 'The lastName must be more than 3 and less than 50 characters long'
												},
												regexp : {
													regexp : /^[a-zA-Z]+$/,
													message : 'The lastName can only consist of alphabetical'
												}
											}
										},
										sSecondName : {
											message : 'The secondName is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 50,
													message : 'The secondName must be more than 3 and less than 50 characters long'
												},
												regexp : {
													regexp : /^[a-zA-Z]+$/,
													message : 'The secondName can only consist of alphabetical'
												}
											}
										},
										sAge : {
											message : 'The age is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 3,
													message : 'The age must be more than 1 and less than 3 characters long'
												},
												regexp : {
													regexp : /^[0-9]+$/,
													message : 'The age can only consist of  number'
												}
											}
										},
										sExperience : {
											message : 'The experience is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 100,
													message : 'The agexperiencee must be more than 1 and less than 100 characters long'
												},
												regexp : {
													regexp : /^[a-zA-Z0-9_]+$/,
													message : 'The experience can only consist of alphabetical, number and underscore'
												}
											}
										},
										sDescription : {
											message : 'The experience is not valid',
											validators : {
												stringLength : {
													min : 0,
													max : 100,
													message : 'The agexperiencee must be more than 1 and less than 100 characters long'
												},
												regexp : {
													regexp : /^[a-zA-Z0-9_]+$/,
													message : 'The experience can only consist of alphabetical, number and underscore'
												}
											}
										}

									}
								});
			});
}(jQuery);
