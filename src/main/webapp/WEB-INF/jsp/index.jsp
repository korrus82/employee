<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">
<link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
<link href="webjars/x-editable-bootstrap3/1.5.1/css/bootstrap-editable.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<title>Employee</title>
<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
<script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
<script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="webjars/x-editable-bootstrap3/1.5.1/js/bootstrap-editable.min.js"></script>
<script src="js/employee.js"></script>
</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<!-- 	<div class="navbar navbar-inverse navbar-static-top" role="navigation"> -->
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="?">Employee</a>
			</div>
			<div class="navbar-collapse">

				<ul class="nav pull-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<button type="button" class="btn btn-default">
								Employee Search <b class="caret"></b>
							</button>
					</a>
						<div class="dropdown-menu" style="padding: 0px; min-width: 300px;">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Employee Search</h3>
								</div>
								<div class="panel-body">
									<form action="index.action" id="searchform" method="get" accept-charset="UTF-8">
										<fieldset>
											<div class="form-group">
												<input class="form-control" placeholder="firstName" name="sFirstName" id="sFirstName" type="text"
													value="${sFirstName}">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="lastName" name="sLastName" id="sLastName" type="text"
													value="${sLastName}">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="secondNamee" name="sSecondName" id="sSecondName" type="text"
													value="${sSecondName}">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="age" name="sAge" id="sAge" type="text" value="${sAge}">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="experience" name="sExperience" id="sExperience" type="text"
													value="${sExperience}">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="description" name="sDescription" id="sDescription" type="text"
													value="${sDescription}">
											</div>

										</fieldset>
										<input class="btn btn-lg btn-success btn-block" type="submit" value="Search">
									</form>
								</div>
							</div>
						</div></li>
				</ul>

				<ul class="nav pull-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<button type="button" class="btn btn-default">
								Employee add <b class="caret"></b>
							</button>
					</a>
						<div class="dropdown-menu" style="padding: 0px; min-width: 300px;">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Registration Employee</h3>
								</div>
								<div class="panel-body">
									<form action="add.action" id="registerform" method="post" accept-charset="UTF-8">
										<fieldset>
											<div class="form-group">
												<input class="form-control" placeholder="firstName" name="firstName" id="firstName" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="lastName" name="lastName" id="lastName" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="secondNamee" name="secondName" id="secondName" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="age" name="age" id="age" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="experience" name="experience" id="experience" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="description" name="description" id="description" type="text">
											</div>

										</fieldset>
										<input class="btn btn-lg btn-success btn-block" type="submit" value="Add">
									</form>
								</div>
							</div>
						</div></li>
				</ul>
			</div>
		</div>
	</div>
	<!--/.nav-collapse -->
	<!-- Begin page content -->
	<div class="container bs-docs-container">
		<div class="pagination-centered">
			<ul class="pagination">
				<li><a href="?page=1"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
				<c:forEach var="each" items="${pageWrapper.getPages()}">
					<c:if test="${each == page}">
						<li class="active"><a href="?page=${each}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}">${each}<span class="sr-only">(current)</span></a></li>
					</c:if>
					<c:if test="${each != page}">
						<li><a href="?page=${each}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}">${each}</a></li>
					</c:if>
				</c:forEach>
				<li><a href="?page=${pagesCount}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
				<li><span>Total: ${pageWrapper.getPagesCount()}</span></li>
			</ul>
		</div>
		<table id="employees" class="table table-striped">
			<thead>
				<tr>
					<th><h4>firstName</h4></th>
					<th><h4>lastName</h4></th>
					<th><h4>secondName</h4></th>
					<th><h4>age</h4></th>
					<th><h4>experience</h4></th>
					<th><h4>description</h4></th>
					<th><h4>Remove</h4></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="each" items="${employeesList}">
					<tr>
						<th><h4>
								<a href="#" id="updateFirstName" data-pk="${each.getEmployeeId()}">${each.getFirstName()}</a>
							</h4></th>
						<th><h4>
								<a href="#" id="updateLastName" data-pk="${each.getEmployeeId()}">${each.getLastName()}</a>
							</h4></th>
						<th><h4>
								<a href="#" id="updateSecondName" data-pk="${each.getEmployeeId()}">${each.getSecondName()}</a>
							</h4></th>
						<th><h4>
								<a href="#" id="updateAge" data-pk="${each.getEmployeeId()}">${each.getAge()}</a>
							</h4></th>
						<th><h4>
								<a href="#" id="updateExperience" data-pk="${each.getEmployeeId()}">${each.getExperience()}</a>
							</h4></th>
						<th><h4>
								<a href="#" id="updateDescription" data-pk="${each.getEmployeeId()}">${each.getDescription()}</a>
							</h4></th>
						<th>
							<form action="delete.action" method="post" accept-charset="UTF-8">
								<input name="employeeId" type="hidden" value="${each.getEmployeeId()}"> <input
									class="btn btn-lg btn-danger btn-block" type="submit" value="X">
							</form>
						</th>
					</tr>
				</c:forEach>
			</tbody>
		</table>


		<div class="pagination-centered">
			<ul class="pagination">
				<li><a href="?page=1"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
				<c:forEach var="each" items="${pageWrapper.getPages()}">
					<c:if test="${each == page}">
						<li class="active"><a href="?page=${each}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}">${each}<span class="sr-only">(current)</span></a></li>
					</c:if>
					<c:if test="${each != page}">
						<li><a href="?page=${each}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}">${each}</a></li>
					</c:if>
				</c:forEach>
				<li><a href="?page=${pagesCount}&sFirstName=${sFirstName}&sLastName=${sLastName}&sSecondName=${sSecondName}&sAge=${sAge}&sExperience=${sExperience}&sDescription=${sDescription}"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
				<li><span>Total: ${pageWrapper.getPagesCount()}</span></li>
			</ul>
		</div>
	</div>
</body>
</html>